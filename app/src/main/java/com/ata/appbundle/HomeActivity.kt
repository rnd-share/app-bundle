package com.ata.appbundle

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ata.core.SessionManager
import com.ata.core.UserRepository
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    //lateinit var binding: ActivityHomeBinding
    lateinit var userRepository: UserRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        binding = ActivityHomeBinding.inflate(layoutInflater)
//        setContentView(binding.root)
        setContentView(R.layout.activity_home)

        val sesi = SessionManager(this)
        userRepository = UserRepository.getInstance(sesi)

        tvWelcome.text = "Welcome ${userRepository.getUser()}"

        btnLogout.setOnClickListener {
            userRepository.logoutUser()
            moveToMainActivity()
        }

        fab.setOnClickListener {
            try {

                moveToChatActivity()
//                installChatModule()
//                val uri = Uri.parse("appbundle://chat")
//                startActivity(Intent(Intent.ACTION_VIEW, uri))
            } catch (e: Exception){
                Toast.makeText(this, "Module not found", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun moveToChatActivity() {
        startActivity(Intent(this, TollActivity::class.java))
    }

    private fun moveToMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

}