package com.ata.appbundle.chat

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.ata.appbundle.BaseSplitActivity
import com.ata.core.SessionManager
import com.ata.core.UserRepository


class ChatActivity : BaseSplitActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        val tvChat = findViewById<TextView>(R.id.tv_chat)
        val btn = findViewById<Button>(R.id.btn_additional)
        val sesi = SessionManager(this)
        val userRepository = UserRepository.getInstance(sesi)
        tvChat.text = "Hello ${userRepository.getUser()}!\n Welcome to Chat Feature"

        btn.setOnClickListener {
            startActivity(Intent(this, AdditionalActivity::class.java))
            //finish()
        }
    }

}