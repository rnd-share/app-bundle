package com.ata.core

import android.os.Parcel
import android.os.Parcelable


data class FilmEntity(
    val title: String?,
    val poster: String?,
    val genre: String?,
    val running_time: String?,
    val original_language: String?,
    val overview: String?,
    val type: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(poster)
        parcel.writeString(genre)
        parcel.writeString(running_time)
        parcel.writeString(original_language)
        parcel.writeString(overview)
        parcel.writeInt(type)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FilmEntity> {
        override fun createFromParcel(parcel: Parcel): FilmEntity {
            return FilmEntity(parcel)
        }

        override fun newArray(size: Int): Array<FilmEntity?> {
            return arrayOfNulls(size)
        }
    }
}